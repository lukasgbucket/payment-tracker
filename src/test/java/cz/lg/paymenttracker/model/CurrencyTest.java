package cz.lg.paymenttracker.model;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class CurrencyTest {

    private static final String USD_CURRENCY_CODE = "USD";
    private static final String HKD_CURRENCY_CODE = "HKD";
    private static final String[] INVALID_CURRENCY_CODES = {"aa", "AA", "aaaa", "AAAA", "AaA", "aaa", "000", "0", "0000"};
    private static final String[] VALID_CURRENCY_CODES = {"USD", "HKD", "RMB"};

    @Test
    public void toStringTest() {
        final Currency currency = Currency.getInstance(USD_CURRENCY_CODE);

        assertEquals("Currency toString method should return currency code set by getInstance method.", USD_CURRENCY_CODE, currency.toString());
    }

    @Test
    public void hashCodeTest() {
        final Currency currencyFoo = Currency.getInstance(USD_CURRENCY_CODE);
        final Currency currencyBar = Currency.getInstance(USD_CURRENCY_CODE);

        assertTrue("Currencies Foo and Bar should have same hashCode.", currencyFoo.hashCode() == currencyBar.hashCode());

        final Currency currencyHkd = Currency.getInstance(HKD_CURRENCY_CODE);

        assertFalse("Currencies Foo and Hkd should have different hashCode.", currencyFoo.hashCode() == currencyHkd.hashCode());
    }

    @Test
    public void objectEqualityTest() {
        final Currency currencyFoo = Currency.getInstance(USD_CURRENCY_CODE);
        final Currency currencyBar = Currency.getInstance(USD_CURRENCY_CODE);

        assertTrue("Currencies with same currency code should have same hashCode. Currency Foo hashCode has to be same as currency Bar hashCode.", currencyFoo.hashCode() == currencyBar.hashCode());

        assertTrue("Currencies with same currency code should be considered as equal. Currency Foo has to be equal as currency Bar.", currencyFoo.equals(currencyBar));
        assertTrue("Accounts with same currency should be considered as equal. Account Bar has to be equal as account Foo.", currencyBar.equals(currencyFoo));

        assertFalse("Currency Bar should not be equal to null.", currencyBar.equals(null));
    }

    @Test
    public void getValidInstanceTest() {
        for(String currencyCode : VALID_CURRENCY_CODES) {
            final Currency currency = Currency.getInstance(currencyCode);

            assertThat("With valid currency code proper Currency object should be instantiated.", currency, instanceOf(Currency.class));
        }
    }

    @Test (expected = IllegalStateException.class)
    public void getInvalidInstanceTest() {
        for(String currencyCode : INVALID_CURRENCY_CODES) {
            final Currency currency = Currency.getInstance(currencyCode);

            assertThat("With INVALID currency code proper Currency object should NOT be instantiated.", currency, instanceOf(Currency.class));
        }
    }

    @Test
    public void getCurrencyCodeTest() {
        final Currency currency = Currency.getInstance(USD_CURRENCY_CODE);

        assertEquals("Method getCurrencyCode should return currency code from which class was instantiated.", USD_CURRENCY_CODE, currency.getCurrencyCode().toString());
    }
}
