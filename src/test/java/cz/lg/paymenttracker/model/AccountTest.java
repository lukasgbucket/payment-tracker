package cz.lg.paymenttracker.model;

import static cz.lg.paymenttracker.constants.Constants.INITIAL_ACCOUNT_BALANCE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashSet;

public class AccountTest
{
    private static final String USD_CURRENCY_CODE = "USD";
    private static final String HKD_CURRENCY_CODE = "HKD";

    @Test
    public void areAccountsEqualTest() {
        final Account usdAccount1 = new Account(Currency.getInstance(USD_CURRENCY_CODE));
        final Payment payment1 = new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal("1000"));
        usdAccount1.updateBalance(payment1);

        final Account usdAccount2 = new Account(Currency.getInstance(USD_CURRENCY_CODE));
        final Payment payment2 = new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal("200"));
        usdAccount2.updateBalance(payment2);

        final Account hkdAccount = new Account(Currency.getInstance(HKD_CURRENCY_CODE));
        final Payment payment3 = new Payment(Currency.getInstance(HKD_CURRENCY_CODE), new BigDecimal("100"));
        hkdAccount.updateBalance(payment3);

        assertTrue("Accounts with same currency code shoud be equal.", usdAccount1.equals(usdAccount2));
        assertFalse("Accounts with different currency should not be equal.", usdAccount1.equals(hkdAccount));
    }

    /**
     * Account has to have overridden methods equals() and hashCode() implemented correctly,
     * so each currency is inserted into HashMap only once.
     */
    @Test
    public void insertionOfAccountToHashSetTest() {
        final HashSet<Account> portfolio = new HashSet<>();

        final Account usdAccount1 = new Account(Currency.getInstance(USD_CURRENCY_CODE));
        final Account usdAccount2 = new Account(Currency.getInstance(USD_CURRENCY_CODE));

        portfolio.add(usdAccount1);
        portfolio.add(usdAccount2);

        assertEquals("Accounts with currency code code USD should be inserted to portfolio only once.", 1, portfolio.size());
    }

    @Test
    public void positiveUpdateBalanceTest() {
        final Account account = new Account(Currency.getInstance(USD_CURRENCY_CODE));
        final Payment payment = new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal("100"));
        account.updateBalance(payment);

        assertEquals("Sending positive payment to newly created account should result in same account balance.", payment.getAmount(), account.getBalance());
    }

    @Test
    public void positiveDoubleUpdateBalanceTest() {
        final Account account = new Account(Currency.getInstance(USD_CURRENCY_CODE));
        final Payment payment1 = new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal("100"));
        account.updateBalance(payment1);

        final Payment payment2 = new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal("-50"));
        account.updateBalance(payment2);

        final BigDecimal totalAmount = (INITIAL_ACCOUNT_BALANCE).add(payment1.getAmount()).add(payment2.getAmount());

        assertEquals("Sending 2 payments to newly created account should result in correct account balance.", totalAmount, account.getBalance());
    }

    @Test
    public void updateBalanceAndPaymentsSuccessTest() {

        final String[] paymentValues = {"1000", "2000", "3000", "-3000", "-2000"};

        final Account accountFoo = new Account(Currency.getInstance(USD_CURRENCY_CODE));

        for(String paymentValue : paymentValues) {
            accountFoo.updateBalance(new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal(paymentValue)));
        }

        assertEquals("There should be 5 payments for Account.", paymentValues.length, accountFoo.getPayments().size());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void updateBalanceAndPaymentsFailureTest() {

        final String[] paymentValues = {"1000", "2000", "3000", "-3000", "-6000"};

        final Account accountFoo = new Account(Currency.getInstance(USD_CURRENCY_CODE));

        for (String paymentValue : paymentValues) {
            accountFoo.updateBalance(new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal(paymentValue)));
        }

        final int expectedNumberOfPayments = paymentValues.length - 1;

        assertEquals(String.format("There should be 5 payments for Account.", expectedNumberOfPayments), expectedNumberOfPayments, accountFoo.getPayments().size());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void updateBalanceAndCheckTotalBalanceWithPaymentsFailureTest() {

        final String[] paymentValues = {"1000", "2000", "3000", "-3000", "-6000"};

        final Account accountFoo = new Account(Currency.getInstance(USD_CURRENCY_CODE));

        for (String paymentValue : paymentValues) {
            accountFoo.updateBalance(new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal(paymentValue)));
        }

        BigDecimal totalAmount = new BigDecimal("0");
        for(Payment payment : accountFoo.getPayments()) {
            totalAmount.add(payment.getAmount());
        }

        assertEquals(String.format("Sum of payments (%s) for this account should be same as account balance (%s).", totalAmount, accountFoo.getBalance()), totalAmount, accountFoo.getBalance());
    }

    @Test
    public void accountCreationTest() {
        final Account account = new Account(Currency.getInstance(USD_CURRENCY_CODE));

        assertEquals("Newly initialized account should have zero balance.", INITIAL_ACCOUNT_BALANCE, account.getBalance());
    }

    @Test
    public void toStringTest() {
        final Account account = new Account(Currency.getInstance(USD_CURRENCY_CODE));
        final Payment payment = new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal("100"));
        account.updateBalance(payment);

        assertEquals("Method toString() of Account object should return account currency ISO4237 code separated with space and then follows the account balance.", USD_CURRENCY_CODE + " " + payment.getAmount(), account.toString());
    }

    @Test
    public void objectEqualityTest() {
        final Account accountFoo = new Account(Currency.getInstance(USD_CURRENCY_CODE));
        final Account accountBar = new Account(Currency.getInstance(USD_CURRENCY_CODE));

        assertTrue("Accounts with same currency should have same hashCode. Account Foo hashCode has to be same as account Bar hashCode.", accountFoo.hashCode() == accountBar.hashCode());

        assertTrue("Accounts with same currency should be considered as equal. Account Foo has to be equal as account Bar.", accountFoo.equals(accountBar));
        assertTrue("Accounts with same currency should be considered as equal. Account Bar has to be equal as account Foo.", accountBar.equals(accountFoo));

        assertFalse("Account bar should not be equal to null.", accountBar.equals(null));
    }
}
