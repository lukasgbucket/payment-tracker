package cz.lg.paymenttracker.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

public class PaymentTest {

    private static final String[] VALID_RAW_INPUTS = {"USD -10", "USD 10", "USD 100", "USD -1", "USD 1"};

    private static final String[] INVALID_RAW_INPUTS = {"UsD 1", "usd 10", "USD -1.0", "USD -10.0", "USD -10.00", "USD -1.00",};

    private static final String USD_CURRENCY_CODE = "USD";

    @Test
    public void toStringTest() {
        final String paymentAmount = "100";
        Payment payment = new Payment(Currency.getInstance(USD_CURRENCY_CODE), new BigDecimal(paymentAmount));

        assertEquals("Method toString() of Payment object should return payment currency ISO4237 code separated with space and then follows the payment amount.", USD_CURRENCY_CODE + " " + paymentAmount, payment.toString());
    }

    @Test
    public void getPaymentFromRawInputValidTest() {
        for(String input : VALID_RAW_INPUTS) {
            assertNotNull(String.format("\"%s\" should be valid raw input.", input), Payment.getPaymentFromRawInput(input));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPaymentFromRawInputInvalidTest() {
        for(String input : INVALID_RAW_INPUTS) {
            assertNotNull(String.format("\"%s\" should be INVALID raw input.", input), Payment.getPaymentFromRawInput(input));
        }
    }
}
