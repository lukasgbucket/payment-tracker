package cz.lg.paymenttracker.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PortfolioTest {

    @Test
    public void creation() {
        final Portfolio portfolio = new Portfolio();

        assertEquals("Newly created portfolio should have zero accounts associated.", 0, portfolio.getAccountsCount());
    }
}
