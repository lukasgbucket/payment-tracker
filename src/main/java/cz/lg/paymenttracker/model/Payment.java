package cz.lg.paymenttracker.model;

import static cz.lg.paymenttracker.constants.Constants.CURRENCY_CODE_REGEXP_PATTERN;
import static cz.lg.paymenttracker.constants.Constants.AMOUNT_REGEXP_PATTERN;
import static cz.lg.paymenttracker.util.Util.isValid;

import java.io.Serializable;
import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;

public class Payment implements Serializable {

    private static final long serialVersionUID = 3180639361560572436L;

    private Currency currency;
    private BigDecimal amount;

    public Payment(final Currency currency, final BigDecimal amount) throws IllegalArgumentException {
        this.currency = currency;
        this.amount = amount;
    }

    @Override
    public String toString() {
        final StringBuilder paymentToString = new StringBuilder();
        paymentToString.append(this.getCurrency().getCurrencyCode()).append(" ").append(this.getAmount());

        return paymentToString.toString();
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setCurrency(final Currency currency) throws IllegalArgumentException {
       this.currency = currency;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    public static Payment getPaymentFromRawInput(final String rawPaymentInput) throws IllegalArgumentException, ArrayIndexOutOfBoundsException {
        try {
            final String[] splitedLine = StringUtils.split(rawPaymentInput);

            final String currencyCode = splitedLine[0];
            final String amountStr = splitedLine[1];

            if(isValid(currencyCode, CURRENCY_CODE_REGEXP_PATTERN) && isValid(amountStr, AMOUNT_REGEXP_PATTERN)) {
                final Payment payment = new Payment(Currency.getInstance(currencyCode), new BigDecimal(amountStr));

                return payment;
            } else {
                throw new IllegalArgumentException(String.format("Invalid payment input \"%s\"", rawPaymentInput));
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ArrayIndexOutOfBoundsException("You, mistyped payment input. Try again please.");
        }
    }
}
