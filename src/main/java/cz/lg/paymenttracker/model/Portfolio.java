package cz.lg.paymenttracker.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Portfolio
 *
 * Portfolio holds multiple accounts with different currencies.
 * There should be only one account of each currency in portfolio.
 */
public class Portfolio implements Serializable {

    private static final long serialVersionUID = 323420758029048647L;

    private HashMap<String, Account> portfolio;

    public Portfolio() {
        this.portfolio = new HashMap<>();
    }

    @Override
    public String toString() {
        StringBuilder portfolioAccountsOverview = new StringBuilder();

        for (Map.Entry<String, Account> portfolioEntry : getPortfolio().entrySet()) {
            final Account account = portfolioEntry.getValue();
            portfolioAccountsOverview.append(account.toString() + "%n");
        }

        return portfolioAccountsOverview.toString();
    }

    public String printPortfolioWithoutZeroBalanceAccounts() {
        StringBuilder portfolioWithoutZeroBalanceAccountsOverview = new StringBuilder();

        for (Map.Entry<String, Account> portfolioEntry : getPortfolio().entrySet()) {
            final Account account = portfolioEntry.getValue();

            if(!account.hasZeroBalance()) {
                portfolioWithoutZeroBalanceAccountsOverview.append(account.toString() + "%n");
            }
        }

        return portfolioWithoutZeroBalanceAccountsOverview.toString();
    }

    public HashMap<String, Account> getPortfolio() {
        return this.portfolio;
    }

    public synchronized void addOrUpdateExistingAccount(final Account account) {
        final String key = account.getCurrency().getCurrencyCode();

        this.getPortfolio().put(key, account);
    }

    public boolean containsAccount(final Currency currency) {
        return this.getPortfolio().containsKey(currency.getCurrencyCode());
    }

    public Account getAccount(final Currency currency) {
        return this.getPortfolio().get(currency.getCurrencyCode());
    }

    public int getAccountsCount() {
        return this.getPortfolio().size();
    }
}
