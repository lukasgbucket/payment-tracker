package cz.lg.paymenttracker.model;

import static cz.lg.paymenttracker.constants.Constants.INITIAL_ACCOUNT_BALANCE;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

public class Account implements Serializable {

    private static final long serialVersionUID = -3369387228516492747L;

    private Currency currency;
    private BigDecimal balance;
    private ArrayList<Payment> payments;

    public Account(final Currency currency) {
        this.currency = currency;
        this.balance = INITIAL_ACCOUNT_BALANCE;
        this.payments = new ArrayList<>();
    }

    @Override
    public boolean equals(final Object obj) {
        if(obj == this) {
            return true;
        }

        if(obj instanceof Account) {
            final Account other = (Account) obj;
            return Objects.equals(other.getCurrency().getCurrencyCode(), this.getCurrency().getCurrencyCode());
        }

        return false;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getCurrency().getCurrencyCode());
    }
    
    @Override
    public final String toString() {
        final StringBuilder accountToString = new StringBuilder();
        accountToString.append(this.getCurrency().getCurrencyCode()).append(" ").append(this.getBalance());

        return accountToString.toString();
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public ArrayList<Payment> getPayments() {
        return payments;
    }

    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    /**
     * Thread safe method by using keyword synchronized.
     */
    public synchronized void updateBalance(final Payment payment) throws UnsupportedOperationException {
        final BigDecimal currentBalance = this.getBalance();

        final BigDecimal newBalance = currentBalance.add(payment.getAmount());

        if(newBalance.compareTo(new BigDecimal("0")) >= 0) {
            this.balance = newBalance;
            this.getPayments().add(payment);
        } else {
            throw new UnsupportedOperationException(String.format("%s account balance %s overthrown by payment of %s %s. Not possible to process this payment!", getCurrency(), currentBalance,  payment.getCurrency(), payment.getAmount()));
        }
    }

    public boolean hasZeroBalance() {
        return INITIAL_ACCOUNT_BALANCE.equals(getBalance());
    }

    private BigDecimal getExchangeRate(final Currency currency) {
        final BigDecimal exchangeRate = new BigDecimal("1.2"); // #TODO fetch some API to get exchange rate
        return exchangeRate;
    }
}
