package cz.lg.paymenttracker.model;

import static cz.lg.paymenttracker.constants.Constants.CURRENCY_CODE_REGEXP_PATTERN;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Objects;

/**
 * Currency
 *
 * java.util.Currency is not going to be used due to fact that enables only ISO 4217 currency codes
 */
public final class Currency implements Serializable {

    private static final long serialVersionUID = -51558886142485179L;

    private String currencyCode;

    private Currency() {
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getCurrencyCode());
    }

    @Override
    public final boolean equals(final Object obj) {
        if(obj == this) {
            return true;
        }

        if(obj instanceof Currency) {
            final Currency other = (Currency) obj;
            return Objects.equals(currencyCode, other.currencyCode);
        }

        return false;
    }

    @Override
    public String toString() {
        return this.currencyCode;
    }

    public static Currency getInstance(final String currencyCode) throws IllegalStateException {
        Currency currency = new Currency();

        currency.setCurrencyCode(currencyCode);

        return currency;
    }

    public void setCurrencyCode(final String currencyCode) throws IllegalStateException {
        if(!isCurrencyCodeValid(currencyCode)) {
            throw new IllegalStateException(String.format("Currency code %s is invalid! Currency code should be 3 uppercase letter characters.", currencyCode));
        }

        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return this.currencyCode;
    }

    private static boolean isCurrencyCodeValid(final String currencyCode) {
        final Pattern pattern = Pattern.compile(CURRENCY_CODE_REGEXP_PATTERN);
        final Matcher matcher = pattern.matcher(currencyCode);

        return matcher.matches();
    }
}
