package cz.lg.paymenttracker.reader;

import cz.lg.paymenttracker.model.Payment;

import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class PaymentsFileReader implements PaymentsReader {

    private String fileName;

    public PaymentsFileReader(final String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    private List<String> getFileLines() throws IOException {
        List<String> fileLines;

        try (Stream<String> stream = Files.lines(Paths.get(getFileName()))) {
            fileLines = stream.collect(Collectors.toList());
        } catch (IOException e) {
            throw new IOException(String.format("Error while collecting lines from file: %s", getFileName()));
        }

        return fileLines;
    }

    @Override
    public ArrayList<Payment> getPayments() throws IllegalArgumentException, IOException {
        final ArrayList<Payment> paymentsList = new ArrayList<>();

        try {
            final List<String> fileLines = getFileLines();

            for (String line : fileLines) {
                try {
                    final Payment payment = Payment.getPaymentFromRawInput(line);

                    paymentsList.add(payment);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(e);
                }
            }

            return paymentsList;
        } catch (IOException e) {
            throw new IOException(e);
        }
    }
}
