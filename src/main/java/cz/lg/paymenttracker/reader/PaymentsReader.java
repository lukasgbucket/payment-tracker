package cz.lg.paymenttracker.reader;

import cz.lg.paymenttracker.model.Payment;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Payments Reader Interface
 *
 * Implement this interface in case you want to preload payments from different sources eg. file, REST, SOAP etc.
 *
 */
public interface PaymentsReader {

    ArrayList<Payment> getPayments() throws IOException;
}
