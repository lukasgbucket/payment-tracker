package cz.lg.paymenttracker.constants;

import java.math.BigDecimal;

public final class Constants {

    private Constants() {
        throw new UnsupportedOperationException("Constants class is not intended to be instantiated. Use static imports for it's instance variables.");
    }

    /**
     *  \-? - optional minus sign
     *  \d+ - digit
     */
    public static final String AMOUNT_REGEXP_PATTERN = "^\\-?\\d+$";

    /**
     * [A-Z]{3} - three uppercase letter characters
     */
    public static final String CURRENCY_CODE_REGEXP_PATTERN = "^[A-Z]{3}$";

    public static final int PERIODICITY_OF_PORTFOLIO_JOB_IN_MILLISECONDS = 60000; // default 60 seconds = 60000 milliseconds

    public static final BigDecimal INITIAL_ACCOUNT_BALANCE = new BigDecimal("0");

    public static final String PROGRAM_TERMINATION_COMMAND = "quit";
}
