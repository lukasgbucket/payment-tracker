package cz.lg.paymenttracker;

import cz.lg.paymenttracker.job.PaymentInputConsoleReaderJob;
import cz.lg.paymenttracker.job.PortfolioConsolePrinterJob;
import cz.lg.paymenttracker.model.Account;
import cz.lg.paymenttracker.model.Payment;
import cz.lg.paymenttracker.model.Portfolio;
import cz.lg.paymenttracker.reader.PaymentsFileReader;
import cz.lg.paymenttracker.reader.PaymentsReader;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.DefaultParser;
import org.fusesource.jansi.AnsiConsole;
import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.BLUE;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.Color.CYAN;

import java.io.IOException;
import java.util.ArrayList;

public final class PaymentTrackerApp
{
    private PaymentTrackerApp() {
    }

    public static void main( final String[] args )
    {
        AnsiConsole.systemInstall();
        System.out.println( ansi().eraseScreen().fg(BLUE).a("Payment Tracker").reset());
        System.out.println( ansi().fg(BLUE).a("type \"quit\" to exit the application").reset());

        Options options = new Options();
        options.addOption("f", "file", true, "specify path to file with existing payment transactions");

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Payment Tracker", options);

        Portfolio portfolio = loadPaymentsFromFile(options, args);

        final Runnable totalBalanceConsolePrinterJob = new PortfolioConsolePrinterJob(portfolio);
        final Thread totalBalanceConsolePrinterThread = new Thread(totalBalanceConsolePrinterJob);
        totalBalanceConsolePrinterThread.setName("Total Balance Console Printer");
        totalBalanceConsolePrinterThread.start();

        final Runnable paymentInputConsoleReaderJob = new PaymentInputConsoleReaderJob(portfolio);
        final Thread paymentInputConsoleReaderThread = new Thread(paymentInputConsoleReaderJob);
        paymentInputConsoleReaderThread.setName("Payment Input Console Reader");
        paymentInputConsoleReaderThread.start();
    }

    private static Portfolio loadPaymentsFromFile(final Options options, final String[] args) {

        CommandLineParser parser = new DefaultParser();

        CommandLine cmd = null;

        try {
            cmd = parser.parse( options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Portfolio portfolio = new Portfolio();

        if(cmd != null && cmd.hasOption("file")) {
            final String fileName = cmd.getOptionValue("f");
            PaymentsReader paymentsReader = new PaymentsFileReader(fileName);

            System.out.println(ansi().fg(CYAN).a("Reading payment transactions from file: ").a(fileName).reset());

            ArrayList<Payment> payments = new ArrayList<>();

            try {
                payments = paymentsReader.getPayments();
            } catch(IllegalArgumentException | IOException e) {
                System.out.println(ansi().fg(RED).a(e.getMessage()).a(e).reset());
            }

            for(Payment payment : payments) {
                System.out.println(payment);

                Account account;

                if( portfolio.containsAccount(payment.getCurrency()) ) {
                    account = portfolio.getAccount(payment.getCurrency());
                } else {
                    account = new Account(payment.getCurrency());
                }

                try {
                    account.updateBalance(payment);
                } catch (UnsupportedOperationException e) {
                    System.out.format(e.getMessage());
                }

                portfolio.addOrUpdateExistingAccount(account);
            }
        }

        return portfolio;
    }
}
