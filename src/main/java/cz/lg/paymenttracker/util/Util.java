package cz.lg.paymenttracker.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Util {

    private Util() {
        throw new UnsupportedOperationException("This class is not intended to be instantiated.");
    }

    public static boolean isValid(final String input, final String regexp) {
        final Pattern pattern = Pattern.compile(regexp);
        final Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }
}
