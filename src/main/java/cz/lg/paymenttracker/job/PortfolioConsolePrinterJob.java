package cz.lg.paymenttracker.job;

import cz.lg.paymenttracker.model.Portfolio;

import static cz.lg.paymenttracker.constants.Constants.PERIODICITY_OF_PORTFOLIO_JOB_IN_MILLISECONDS;
import static org.fusesource.jansi.Ansi.Color.YELLOW;
import static org.fusesource.jansi.Ansi.ansi;

/**
 * Job (running in thread) which prints total balance for each currency.
 */
public final class PortfolioConsolePrinterJob implements Runnable {

    private Portfolio portfolio;

    public PortfolioConsolePrinterJob(final Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    @Override
    public void run() {
        printPortfolioPeriodically(PERIODICITY_OF_PORTFOLIO_JOB_IN_MILLISECONDS);
    }

    private void printPortfolioPeriodically(final int timeInMilliseconds) {
        while(true) {
            try {
                Thread.sleep(timeInMilliseconds);
            } catch(InterruptedException ex) {
                ex.printStackTrace();
            }

            printPortfolio();
        }
    }

    private void printPortfolio() {
        System.out.println(ansi().fg(YELLOW).a("Portfolio overview:").reset());
        System.out.format(this.getPortfolio().printPortfolioWithoutZeroBalanceAccounts());
    }
}
