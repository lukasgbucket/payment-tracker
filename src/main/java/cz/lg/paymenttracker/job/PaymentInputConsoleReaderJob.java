package cz.lg.paymenttracker.job;

import cz.lg.paymenttracker.model.Account;
import cz.lg.paymenttracker.model.Payment;
import cz.lg.paymenttracker.model.Portfolio;
import static cz.lg.paymenttracker.constants.Constants.PROGRAM_TERMINATION_COMMAND;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.CYAN;

/**
 * Job (running in thread) which receives payment input from user. User could also quit app through this UI.
 */
public final class PaymentInputConsoleReaderJob implements Runnable {

    private Portfolio portfolio;

    public PaymentInputConsoleReaderJob(final Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    @Override
    public void run() {
        readInputFromConsole();
    }

    private void readInputFromConsole() {
        while(true) {
            readSingleInputFromConsole();
        }
    }

    /**
     * Receives payment transaction from console UI and adds payment to correct account. Account is added to portfolio.
     */
    private void readSingleInputFromConsole() {
        System.out.println(ansi().fg(CYAN).a("Please enter new payment transaction eg. \"USD 1000\", \"GBP -200\" etc. and then hit [Enter]:").reset());

        final String rawPaymentInput = System.console().readLine();

        if(PROGRAM_TERMINATION_COMMAND.equals(rawPaymentInput)) {
            System.exit(1);
        }

        try {
            final Payment payment = Payment.getPaymentFromRawInput(rawPaymentInput);

            System.out.println(ansi().fg(GREEN).a("Recieved payment from console: ").a(payment).reset());

            Account account = this.getPortfolio().getAccount(payment.getCurrency());

            if(account == null) { // if account doesn't exist in portfolio yet, then create new one
                account = new Account(payment.getCurrency());
            }

            account.updateBalance(payment);
            this.getPortfolio().addOrUpdateExistingAccount(account);
        } catch (IllegalStateException | UnsupportedOperationException | ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
            System.out.println(ansi().fg(RED).a(e.getMessage()).reset());
        }
    }
}
