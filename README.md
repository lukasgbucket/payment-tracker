# Payment Tracker #

Java based project which is part of an backend developer job interview at fintech IT system integrator company.

![Payment Tracker](screenshot.png?raw=true)

## Development ###

### Requirements ###

* Java 8
* Apache Maven 3.5.0

### Building ###

Go to folder where ```pom.xml``` is located and run command:
```
mvn clean install assembly:single
```

### Running application ###

After build is done go to ```target``` folder and run command:
```
java -jar payment-tracker-1.0.0-jar-with-dependencies.jar
```

For pre-loaded payments from file use:
```
java -jar payment-tracker-1.0.0-jar-with-dependencies.jar -f ../sample-input-file.txt
```

### Generating FindBugs™ reports ###

Use ```mvn site``` command to generate FindBugs reports.
Generated reports could be found at ```/target/site/findbugs.html```.

## Assumptions and my thoughts ##

* If I look at input an output it's obvious that inputs are payments and outputs are balances (sums) in for each currency.

* I assume that account balance shouldn't be negative.

* I use BigDecimal instead of primitive type double while working with money due to inexactness of double (which also has precision of 2 denominators).

* I use Apache Commons CLI library due to fact that it enables me to focus on business logic instead of reinventing the wheel again (which also applies on the rest of the frameworks and the libraries used in this project).

* I use one big JAR which has all it's dependencies inside which may be not the best solution for project which consists multiple modules which shares same dependencies, but I've choose this approach for this project so the reviewer will be able to build and run the project without any issues.

* I've used Console over Scanner (does not have synchronized methods) and BufferedReader (old and unfriendly) for retrieving input from user.

* I did not used Spring Framework with Dependency Injection and I've handled creation of objects by myself (Spring uses Inversion of Control) due to fact that what I understand from exercise specification it's not recommended to use many libraries and for small task such as this it's not necessary.